<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $products = Product::orderBy('created_at', 'asc')->take(10)->get();
        $categories = Category::all();
        return view('home',[
            'products' => $products,'categories' => $categories
        ]);
    }

    //authorize admin
    public function admin()
    {
        $this->authorize('admin');
        
    }

    //display products of a certain category
    public function display_category_products($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        $products = Product::where('category_id', $id)->get();
        return view('shopping/display_category',[
            'products' => $products,'category'=>$category,'categories' => $categories]);
    }

    //display products of all categories
    public function display_all(){
        $products = Product::orderBy('created_at', 'asc')->get();
        $categories = Category::all();
        return view('home',[
            'products' => $products,'categories' => $categories
        ]);

    }

    //display order page with user's order details
    public function display_order(){
        $user = Auth::User();
        $orders = Order::where('userId', $user->id)->get();
        return view('shopping/order', ['orders'=> $orders]);

    }
    //show contact page
    public function contact(){

        return view('pages/contact');
    }
    //show about page
    public function about(){

        return view('pages/about');
    }

}
