<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderController extends Controller
{
    //
    public function construct(){
        $this-> middleware('auth');

    }

    public function index(){
        $this->authorize('admin');
        return view('order.index', ['orders' => Order::all()]);}
    

    public function shipped($id){
        $order = Order::find($id);
        $order ->status='shipped';
        $order->save();

        return redirect('order');
    }
    
    



}
