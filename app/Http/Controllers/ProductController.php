<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 6/06/16
 * Time: 18:04
 */
namespace App\Http\Controllers;


use App\Category;
use App\Product;
use App\Supplier;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    //

    public function index(){

        return view('product/index',[
            'products' => Product::all()
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' =>'required'

        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;

        $product->category_id=$request->category;
        $product->supplier_id=$request->supplier;


        if($request->hasFile('file')){

            $file = $request->file('file');
            $product->picture='uploads/'.$file->getClientOriginalName();
            $file->move('uploads', $file->getClientOriginalName());

        }
        $product->save();

        return redirect('product');

    }

    public function create(){
        return view('product/create', [
            'categories' => Category::lists('name', 'id'), 'suppliers'=> Supplier::lists('name','id')
        ]);
    }

    public function delete($id){
        $product = Product::find($id);

        $product->delete();
        return redirect('product');
    }





}
