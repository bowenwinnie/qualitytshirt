<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Product;
use App\ShoppingCart;
use App\ShoppingCartItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    //
    public function index(Request $request){

        $cart = $request->session()->get('cart', new ShoppingCart());
        $total=0.0;
        foreach ($cart->cartItems as $cart_item){
            $total += $cart_item->product->price*$cart_item->quantity;
        }
        return view("shopping.cart",['cart'=>$cart,'total'=>$total]);

    }

    public function add(Request $request,$id){
        $cart = $request->session()->get('cart', new ShoppingCart());
        $product = Product::find($id);
        $cartItem = null;
        foreach ($cart->cartItems as $cart_item){
            if($cart_item->product->id==$id){

                $cartItem = $cart_item;
                break;
            }
        }
        if($cartItem ==null){
            $cartItem = new ShoppingCartItem();
            $cartItem->product = $product;
            $cartItem->quantity=1;
            array_push($cart->cartItems,$cartItem);
        }
        else{
            $cartItem->quantity+=1;
        }

        $request->session()->put('cart',$cart);
        return redirect('cart');
    }


    public function checkout(Request $request){

        if($request->session()->has('cart')){

            $cart = $request->session()->get('cart',new ShoppingCart());

            if(count($cart->cartItems)>0){


                $subTotalPrice=0.0;
                $order=new Order();
                $user=Auth::User();
                $order->userId=$user->id;
                $order->name=$user->name;
                $order->email=$user->email;
                $order->phone=$user->mobile;
                $order->address=$user->address;

                $order->payed=false;
                $order->status='waiting';
                $order->save();



                foreach ($cart->cartItems as $cart_item){
                    $orderItem = new OrderItem();
                    $orderItem->product_id=$cart_item->quantity;
                    $orderItem->quantity = $cart_item->quantity;
                    $orderItem->price = $cart_item->product->price;
                    $order->orderItems()->save($orderItem);
                    $subTotalPrice+=$cart_item->product->price*$cart_item->quantity;
                    
                }
                $order->subTotalPrice=$subTotalPrice;
                $order->totalPrice=$subTotalPrice*1.15;
                $order->save();

                return view('shopping.checkout', ['order'=>$order]);
               // return redirect('cart');

            }
        }
    }

    public function delete(Request $request,$id){
        
        $cart = $request->session()->get('cart', new ShoppingCart());

        for($i=0;$i<count($cart->cartItems);$i++){
            if($cart->cartItems[$i]->product->id==$id)
            {
                unset($cart->cartItems[$i]);
                break;
            }
        }

        $request->session()->put('cart',$cart);
        return redirect('cart');
    }

    public function clear(Request $request){
        $request->session()->forget('cart');
        return redirect('cart');
    }
}
