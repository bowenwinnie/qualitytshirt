<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    //
    public function about()
    {
        $name = 'winnie';
        $last = 'sun';


        $data = [];
        $data['name'] ='winnie';
        $data['last'] = 'big';

//        return view('pages.about', compact('name', 'last'));
        return view('pages.about', $data);
    }
    public function contact(){
        return view('pages.contact');
    }
}
