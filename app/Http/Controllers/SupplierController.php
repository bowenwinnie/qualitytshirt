<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 6/06/16
 * Time: 16:42
 */

namespace App\Http\Controllers;


use App\Supplier;
use Illuminate\Http\Request;

use App\Http\Requests;

class SupplierController extends Controller
{
    //

    public function index(){

        return view('supplier/index',[
            'suppliers' => Supplier::all()
        ]);
    }

    public function create(){
        return view('supplier/create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'email'=> 'Required|Between:3,64|Email|Unique:users'
        ]);
        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->mobilePhone = $request->mobilePhone;
        $supplier->workPhone = $request->workPhone;
        $supplier->homePhone = $request->homePhone;
        $supplier->address = $request->address;

        $supplier->save();

        return redirect('supplier');
    }


}
