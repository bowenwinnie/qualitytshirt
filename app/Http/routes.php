<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/','HomeController@index');
Route::post('/authenticate', 'Auth\AuthController@authenticate');

Route::get('/home', 'HomeController@index');

Route::resource('category', 'CategoryController');
Route::resource('supplier', 'SupplierController');
Route::resource('product', 'ProductController');

Route::get('product/delete/{id}', 'ProductController@delete');
Route::get('category/delete/{id}', 'CategoryController@delete');

Route::get('about', 'PageController@about');


Route::get('/display_category/{id}', 'HomeController@display_category_products');
Route::get('/display_all', 'HomeController@display_all');



Route::get('/admin', [ 'middleware' => 'auth','uses'=>'HomeController@admin']);
Route::get('/orders',[ 'middleware' => 'auth','uses'=>'HomeController@display_order'] );
Route::resource('user', 'UserController');
Route::get('user/enable/{id}', 'UserController@enable');
Route::get('user/disable/{id}', 'UserController@disable');
Route::get('/view_product/{id}', 'HomeController@viewProduct');

Route::get('/add_cart/{id}', 'CartController@add');
Route::get('/delete/{id}', 'CartController@delete');
Route::get('/clear', 'CartController@clear');
Route::get('/cart', 'CartController@index');
Route::get('/checkout',['middleware' =>'auth','uses'=>'CartController@checkout']);

Route::resource('/order', 'OrderController');
Route::get('/order/shipped/{id}', 'OrderController@shipped');
Route::get('/contact','HomeController@contact');
Route::get('/about', 'HomeController@about');