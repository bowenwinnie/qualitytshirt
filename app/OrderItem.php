<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 14/06/16
 * Time: 00:59
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    public function order(){

        return $this->belongsTo('App/Order');
    } 
}