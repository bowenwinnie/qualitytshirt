<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    /**
     * @return array
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }
    
}
