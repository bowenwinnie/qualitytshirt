<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 13/06/16
 * Time: 17:19
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public  function orderItems(){
        return $this->hasMany('App\OrderItem');

    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}