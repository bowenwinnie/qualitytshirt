<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 14/06/16
 * Time: 01:20
 *
 * when products successfully be checked out, show this page
 */
?>

@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top: 80px; text-align: center">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                    <div><h2>Checkout</h2></div>

                    <div align="center">
                        Thank you for shopping with us
                        <p>
                            Your Order has been placed. Enjoy your day!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection