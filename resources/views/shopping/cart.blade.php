<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 12/06/16
 * Time: 16:07
 *
 * show the order items in shopping cart
 */?>


@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top: 80px">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">My Cart</div>
                    <div class="panel-body">
                        @if (count($cart->cartItems) > 0)
                            <table class="table">

                                <!-- Table titles -->
                                <thead>
                                <th>Picture</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Delete</th>
                                </thead>

                                <tbody>
                                {{--show every order items in teh showing cart--}}
                                @foreach ($cart->cartItems as $cartItem)
                                    <tr>
                                        <td>
                                            <div><img src="{{url($cartItem->product->picture)}}"  style="width:200px;"></div>
                                        </td>
                                        <td >
                                            <div>{{ $cartItem->product->name }}</div>
                                        </td>
                                        <td>
                                            <div>${{ $cartItem->product->price }}</div>
                                        </td>
                                        <td>
                                            <div>{{ $cartItem->quantity }}</div>
                                        </td>
                                        <td >
                                            <div>${{ $cartItem->product->price*$cartItem->quantity }}</div>
                                        </td>
                                        <td>
                                            <a href="{{url('delete/'.$cartItem->product->id) }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Total Without GST</td>
                                    <td><b>${{$total}}</b></td>
                                    <td></td>

                                    <td>Total With GST</td>
                                    <td><b>${{$total*1.15}}</b></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><div><a href="{{url('clear')}}">Clear</a></div></td>
                                    <td><div><a href="{{url('checkout')}}">CheckOut</a></div></td>
                                </tr>
                                </tbody>
                            </table>
                        @else
                            <div align="center">No Products in My Cart</div>

                        @endif

                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
