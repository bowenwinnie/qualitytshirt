<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 12/06/16
 * Time: 16:07
 * show orders that a login user have
 */
?>
@extends('layouts/app')

@section('content')
    <div class="col-md-9" style="margin: 2% 10%">
        <h2>User Details</h2>
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Mobile Phone</th>
                <th>Work phone</th>
                <th>Home Phone</th>
                <th>Address</th>

            </tr>
            <tr>
                <td>
                    <div>{{ Auth::User()->name }}</div>
                </td>

                <td>
                    <div>{{ Auth::User()->username }}</div>
                </td>

                <td>
                    <div>{{ Auth::User()->email }}</div>
                </td>

                <td>
                    <div>{{ Auth::User()-> mobile}}</div>
                </td>

                <td>
                    <div>{{ Auth::User()->phone_work }}</div>
                </td>

                <td>
                    <div>{{ Auth::User()->phone_home }}</div>
                </td>

                <td>
                    <div>{{ Auth::User()->address }}</div>
                </td>
            </tr>
        </table>

    </div>


    <div class="col-md-9" style="margin: 0 10%">
        <h2>Order</h2>
        @if(count($orders)>0)
            <table class="table">
                <tr>
                {{--table's title--}}
                    <th>OrderID</th>
                    <th>User Name</th>
                    <th>Status</th>
                    <th>Payed</th>
                    <th>SubTotal Price</th>
                    <th>Total Price</th>

                </tr>


                {{--show order details under user's accoount--}}
                @foreach($orders as $order)
                    <tr class="table-text">
                        <td><div>{{$order->id}}</div></td>
                        <td><div>{{$order->name}}</div></td>
                        <td><div>{{$order->status}}</div></td>
                        <td class="table-text">
                            @if($order->payed)
                                <div>Yes</div>
                            @else
                                <div>No</div>
                            @endif
                        </td>
                        <td><div>{{$order->subTotalPrice}}</div></td>
                        <td><div>{{$order->totalPrice}}</div></td>
                    </tr>

                @endforeach
            </table>
        @endif
    </div>
@endsection

