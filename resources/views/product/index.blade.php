<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 6/06/16
 * Time: 16:42
 */
?>
@extends('layouts.app')

@section('content')

    <div id="nav" style="margin-top: 80px; text-align: center">
        <ul>
            <li style="display: inline; padding-right:5%"><a href="{{url('product')}}">Manage Products</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('category')}}">Manage Category</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('user')}}">Manage User</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('supplier')}}">Manage Supplier</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('order')}}">Manage Order</a></li>
        </ul>
    </div>
    <div class="col-md-9" style="margin: 2% 10%">
    <h2>Product</h2>

    <p>
        <a href="{{'product/create'}}">Create product</a>
    </p>
    <table class="table">
        <tr>
            <th>
                Name
            </th>
            <th>
                Description
            </th>
            <th>
                Price
            </th>
            <th>
                Category
            </th>
            <th>
                Supplier
            </th>
            <th>
                Image
            </th>
            <th></th>
        </tr>


        @foreach ($products as $product)
            <tr>
                <td>
                    <div>{{ $product->name }}</div>
                </td>
                <td>
                    <div>{{ $product->description }}</div>
                </td>
                <td>
                    <div>{{ $product->price }}</div>
                </td>
                <td>
                    <div>{{ $product->category['name']}}</div>
                </td>
                <td>
                    <div>{{ $product->supplier['name']}}</div>
                </td>
                <td>
                    <div><img src="{{ $product->picture }}" style="height:150px;"></div>
                </td>

                <td>
                    <a href="{{'product/delete/'.$product->id }}">Delete</a>

                </td>
            </tr>
        @endforeach

    </table>
@endsection
