<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 11/06/16
 * Time: 23:35
 */
        ?>

@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top: 40px;">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <h2 style="text-align: center; margin-bottom:30px">Contact Us</h2>

                <address style="font-size: 20px">
                    Quality T-Shirts<br />
                    Unitec, Auckland, New Zealand<br />
                    <abbr title="Phone">P:</abbr>
                    02102220975
                </address>

                <address>
                    <strong>Support:</strong>   <a href="mailto:Support@unitec.com">sunb11@myunitec.com</a><br />

                </address>

            </div>
        </div>
    </div>


@endsection
