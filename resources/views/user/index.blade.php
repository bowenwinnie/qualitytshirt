<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 15/06/16
 * Time: 11:57
 */
        ?>
@extends('layouts.app')

@section('content')


    <div class="container">
        <div id="nav" style="margin-top: 80px; text-align: center">
            <ul>
                <li style="display: inline; padding-right:5%"><a href="{{url('product')}}">Manage Products</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('category')}}">Manage Category</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('user')}}">Manage User</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('supplier')}}">Manage Supplier</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('order')}}">Manage Order</a></li>
            </ul>
        </div>

        <div class="col-md-9" style="margin: 2% 10%">
            <h2>Manage User</h2>

                <table class="table">

                    <!-- Table Headings -->
                    <thead>
                    <th>User Id</th>
                    <th>User Name</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone(home)</th>
                    <th>Phone(work)</th>
                    <th>Mobile</th>
                    <th>Account Type</th>
                    <th>Account Statue</th>
                    <th>Manage Account</th>

                    </thead>

                    <!-- Table Body -->
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td><div>{{ $user->id }}</div></td>
                            <td><div>{{ $user->username}}</div></td>
                            <td><div>{{ $user->name }}</div></td>
                            <td><div>{{ $user->email }}</div></td>
                            <td><div>{{ $user->address }}</div></td>
                            <td><div>{{ $user->phone_home}}</div></td>
                            <td><div>{{ $user->phone_work}}</div></td>
                            <td><div>{{ $user->mobile}}</div></td>
                            <td><div>{{ $user->type}}</div></td>

                            <td>
                                <div>
                                    @if($user->active)
                                        Enabled
                                    @else
                                        Disabled
                                    @endif

                                </div>
                            </td>
                            <td>

                                {{--<a href="{{url('user/'.$user->id)}}">View</a>--}}
                                @if($user->active)
                                    <a href="{{'user/disable/'.$user->id}}">Disable</a>

                                @else
                                    <a href="{{url('/user/enable/'.$user->id) }}">Enable</a>
                                @endif
                            </td>





                        </tr>
                    @endforeach
                    </tbody>
                </table>

        </div>

    </div>


@endsection

