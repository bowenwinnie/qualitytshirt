<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 6/06/16
 * Time: 16:42
 */
?>
@extends('layouts.app')

@section('content')

    <div class="container">
        <div id="nav" style="margin-top: 80px; text-align: center">
            <ul>
                <li style="display: inline; padding-right:5%"><a href="{{url('product')}}">Manage Products</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('category')}}">Manage Category</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('user')}}">Manage User</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('supplier')}}">Manage Supplier</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('order')}}">Manage Order</a></li>
            </ul>
        </div>

        <div class="col-md-9" style="margin: 2% 10%">
        <h2>Category</h2>
        <p>
            <a href="{{url('supplier/create')}}">Create supplier</a>
        </p>

        <table class="table">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    Mobile Phone
                </th>
                <th>
                    Work Phone
                </th>
                <th>
                    Home Phone
                </th>
                <th>
                    Address
                </th>

                <th></th>
            </tr>


            @foreach ($suppliers as $supplier)
                <tr>
                    <td>
                        <div>{{ $supplier->name }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->email }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->mobilePhone }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->workPhone }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->homePhone }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->address }}</div>
                    </td>

                    <td>
                        <a href="{{url('/supplier/delete/'.$supplier->id)}}">Delete</a>

                    </td>
                </tr>
            @endforeach

        </table>
    </div>
@endsection
