<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 13/06/16
 * Time: 16:49
 */
?>

@extends('layouts.app')

@section('content')


    <div class="container" >
        <div id="nav" style="margin-top: 80px; text-align: center">
            <ul>
                <li style="display: inline; padding-right:5%"><a href="{{url('product')}}">Manage Products</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('category')}}">Manage Category</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('user')}}">Manage User</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('supplier')}}">Manage Supplier</a></li>
                <li style="display: inline; padding-right:5%"><a href="{{url('order')}}">Manage Order</a></li>
            </ul>
        </div>

        <div class="col-md-9" style="margin: 2% 10%">
            <h2>Manage Order</h2>
            @if (count($orders) > 0)
                <table class="table">

                    <!-- Table Headings -->
                    <thead>
                    <th>Order Id</th>
                    <th>Name</th>
                    <th>Order Date</th>
                    <th>Status</th>
                    <th>Price</th>
                    <th>Manage status</th>

                    </thead>

                    <!-- Table Body -->
                    <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <!-- show title of the table-->
                            <td><div>{{ $order->id }}</div></td>
                            <td><div>{{ $order->name }}</div></td>
                            <td><div>{{ $order->created_at }}</div></td>
                            <td><div>{{ $order->status}}</div></td>

                            <td><div>${{ $order->totalPrice }}</div></td>
                            <td>
                                @if(strcasecmp($order->status,'Shipped')!=0 )
                                    <a href="{{'order/shipped/'.$order->id }}">Shipped</a>
                                @endif


                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>


@endsection
