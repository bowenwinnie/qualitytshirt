<?php
/**
 * Created by PhpStorm.
 * User: sunbowen
 * Date: 6/06/16
 * Time: 16:03
 */
  ?>
@extends('layouts.app')

@section('content')


    <div id="nav" style="margin-top: 80px; text-align: center">
        <ul>
            <li style="display: inline; padding-right:5%"><a href="{{url('product')}}">Manage Products</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('category')}}">Manage Category</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('user')}}">Manage User</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('supplier')}}">Manage Supplier</a></li>
            <li style="display: inline; padding-right:5%"><a href="{{url('order')}}">Manage Order</a></li>
        </ul>
    </div>

    <div class="col-md-9" style="margin: 2% 10%">
    <h2>Category</h2>
        <div divstyle="font-size: medium">
            {{--<a href="/category/create" >Create category</a>--}}
            <a href="{{url('category/create')}}">Create Category</a>
        </div>



        <table class="table">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Description
                </th>

                <th></th>
            </tr>


            @foreach ($categories as $category)
                <tr>
                    <td>
                        <div>{{ $category->name }}</div>
                    </td>
                    <td>
                        <div>{{ $category->description }}</div>
                    </td>
                    <td>
                        {{--<a href="/category/delete/{{ $category->id }}">Delete</a>--}}

                        <a href="{{url('category/delete/'.$category->id) }}">Delete</a>

                    </td>
                </tr>
            @endforeach

        </table>
    </div>
@endsection
