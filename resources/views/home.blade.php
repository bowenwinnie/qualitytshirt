@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 80px">

    <div class="row ">
        <div class="col-md-3">
            <p class="lead">Quality T-Shirts</p>
            <div class="list-group">

                <a href="#" class="list-group-item">ALL</a>
                @foreach ($categories as $category)
                <a href="{{url('display_category/'.$category->id)}}" class="list-group-item">{{$category->name}}</a>
                @endforeach

            </div>
        </div>

        <div style="display:inline-block" class="col-md-9">
            @foreach ($products as $product)
                <div class ="thumbnail" style="display:inline-block" >

                    <div><img src="{{url($product->picture) }}" style="width:200px;"></div>

                    <div style="font: bold 20px Georgia, serif">{{ $product->name }}</div>
                    <div style="font:15px Georgia,serif">{{ $product->category['name']}}</div>
                    <div style="font-style:oblique">${{$product->price}}</div>

                    <div><a href="{{url('/add_cart/'.$product->id)}}" id="{{$product->id}}" class="btn btn-primary">Add to Cart</a></div>
                </div>

            @endforeach
        </div>
        </div>
    </div>

</div>
@endsection
